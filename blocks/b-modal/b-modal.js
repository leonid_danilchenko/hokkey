$(function(){
    var interval, curr = 0, list = false;
    var prev = function(){
        if (curr > 0) {
            curr --;
            open(list.eq(curr));
        } 
    };
    var next = function(){
        if (curr < list.length - 1) {
            curr++;
            open(list.eq(curr));
        }
    };
    
    var play = function(){
        $('.b-modal__ctrl')
            .removeClass('b-modal__ctrl_state_pause')
            .addClass('b-modal__ctrl_state_play');
        interval = setInterval(next, 2000);
    };
    var pause = function(){
        $('.b-modal__ctrl')
            .removeClass('b-modal__ctrl_state_play')
            .addClass('b-modal__ctrl_state_pause');
        clearInterval(interval);
    };
    var open = function($el, e){
        if (e) {
            e.stopPropagation();
            e.preventDefault();
        }
        if (list.length >= 1) {
            $('.b-modal__ctrl').show();
            curr = list.index($el);
            $('.b-modal__ctrl, .b-modal__arr').show();
            
            if(curr == 0) {
                $('.b-modal__arr_dir_prev').hide();
            } else {
                $('.b-modal__arr_dir_prev').show();
            }
            if(curr == list.length-1) {
                $('.b-modal__arr_dir_next').hide();
            } else {
                $('.b-modal__arr_dir_next').show();
            }
        } else {
            $('.b-modal__ctrl, .b-modal__arr').hide();
        }
        
        
        var txt = '';
        txt = $el.attr('title');
        var src = $el.data('src') || $el.attr('src');

        $('.b-modal, .b-modal-overlay')
            .show()
            .find('.b-modal__subscr')
            .text(txt)
            .closest('.b-modal')
            .find('img')
            .attr('src', src);
    };
   
    var toCenter = function(){
        var $w = $('.b-modal');
        var st = $(window).scrollTop();
        var h = $(window).height();
        var wh = $w.height();
        $w.css({
            'top': wh>h?st:st+(h-wh)/2});
    };

    $('.b-modal__close, .b-modal-overlay, .b-modal').on('click', function(e){
        e.stopPropagation();
        $('.b-modal, .b-modal-overlay').hide();
        pause();
    });

    $(document).on('keyup', function(e){
        if(e.keyCode == '27') {
            $('.b-modal, .b-modal-overlay').hide();
            pause();
        }
        if(e.keyCode == '37' && e.shiftKey) {
            pause();
            prev();
            e.preventDefault();
            e.stopPropagation();
        }
        if(e.keyCode == '39' && e.shiftKey) {
            pause();
            next();
            e.preventDefault();
            e.stopPropagation();
        }
    });
    
    $('.b-modal__wrapper').on('click', function(e){
        e.stopPropagation();
    });
    
    $('.b-modal__ctrl').on('click', function(e){
        if ($(this).hasClass('b-modal__ctrl_state_pause')){
            play();
        } else {
            pause();
        }
        e.preventDefault();
        e.stopPropagation();
    });
    
    $('.js-modal').on('click', function(e){
        list = $(this).closest('.js-modal__list').find('.js-modal');
        open($(this),e);
        $('.b-modal img').one('load', toCenter);
    });
    
    $('.b-modal__arr_dir_prev').on('click', function(){
        pause();
        prev();
    });
    $('.b-modal__arr_dir_next').on('click', function(){
        pause();
        next();
    });
});