module.exports = (grunt) ->
  'use strict'
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  @initConfig


    sprite:
      all:
        src:       ['blocks/**/*_sprite.png']     # Sprite files to read in
        destImg:   'publish/sprite.png'           # Location to output spritesheet
        destCSS:   'blocks/sprite_positions.styl' # Stylus with variables under sprite names
        # imgPath:   'sprite.png' # OPTIONAL: Manual override for imgPath specified in CSS
        algorithm: 'top-down'   # top-down, left-right, diagonal, alt-diagonal, binary-tree [best packing]
        engine:    'phantomjs'  # OPTIONAL: Specify engine (auto, canvas, gm)
        cssFormat: 'stylus'     # OPTIONAL: Specify CSS format (inferred from destCSS' extension by default) (stylus, scss, sass, less, json)
        imgOpts:                # OPTIONAL: Specify img options
          format: 'png'         # Format of the image (inferred from destImg' extension by default) (jpg, png)
          # quality: 90         # Quality of image (gm only)


    connect:
      server:
        options:
          port: 8000,
          base: ''
          middleware: (connect, options) ->
            return [
              # Serve static files.
              connect.static(options.base)
              # Show only html files and folders.
              connect.directory(options.base, { hidden:false, icons:true, filter:(file) ->
                return /\.html/.test(file) || !/\./.test(file);
              })
            ]


    copy:
      images:
        files: [{
          expand:  true
          flatten: true
          cwd:     'blocks',
          src:     ['**/*.{png,jpg,jpeg,gif}', '!**/*_sprite.{png,jpg,jpeg,gif}']
          dest:    'publish'
        }]
      assets:
        files: [{
          expand:  true
          flatten: true
          cwd:     'blocks',
          src:     '**/*.{ttf,eot,svg,woff}'
          dest:    'publish'
        }]


    clean:
      pubimages:
        src: [
          "publish/*.{png,gif,jpg,jpeg}",
          "!publish/sprite.png"
        ]


    imagemin:
      options:
        optimizationLevel: 5
      dist:
        files: [
          {
            expand: true
            cwd:    'publish/'
            src:    '**/*.{png,jpg,jpeg}'
            dest:   'publish/'
          },
          {
            expand: true
            cwd:    'tmp/'
            src:    '**/*.{png,jpg,jpeg}'
            dest:   'tmp/'
          },
          {
            expand: true
            cwd:    './'
            src:    '*.{png,jpg,jpeg,ico}'
            dest:   './'
          },
        ]


    bower:
      install:
        options:
          targetDir:      './lib'
          layout:         'byType'
          install:        true
          verbose:        false
          cleanTargetDir: false
          cleanBowerDir:  true


    concat:
      js:
        src: [
          'lib/consoleshiv.js',
          'lib/**/*.js',
          '!lib/jquery/*.js',
          '!lib/html5shiv/*.js',
          'blocks/**/*.js'
        ]
        dest: 'publish/script.js'

      css:
        src: [
          'blocks/i-reset/i-reset.css',
          'lib/**/*.css',
          '!lib/**/*.ie.css',
          'blocks/_b-*/**/*.css',
          'blocks/b-*/**/*.css',
          '!blocks/_b-*/**/*.ie.css',
          '!blocks/b-*/**/*.ie.css',
          '!blocks/i-*/'
        ]
        dest: 'publish/style.css'

      css_ie:
        src: [
          'blocks/i-reset/i-reset.ie.css',
          'lib/**/*.ie.css',
          'blocks/_b-*/**/*.ie.css',
          'blocks/b-*/**/*.ie.css',
          '!blocks/i-*/'
        ]
        dest: 'publish/style.ie.css'


    uglify:
      dist:
        files:
          '<%= concat.js.dest %>': ['<%= concat.js.dest %>']


    jshint:
      files: [
        'blocks/**/*.js'
      ]
      options:
        curly:    true
        eqeqeq:   true
        eqnull:   true
        # quotmark: true
        undef:    true
        unused:   false

        browser:  true
        jquery:   true
        globals:
          console: true


    watch:
      options:
        livereload: false
        spawn:      false

      stylus:
        options:
          livereload: true
        files: [
          'blocks/**/*.styl',
          '!blocks/**/*.ie.styl'
        ]
        tasks: ['newer:stylus:dev', 'newer:concat:css','newer:autoprefixer']

      stylus_ie:
        options:
          livereload: true
        files: [
          'blocks/**/*.ie.styl',
        ]
        tasks: ['newer:stylus:dev_ie', 'newer:concat:css_ie','newer:autoprefixer']

      js:
        options:
          livereload: true
        files: [
          'lib/**/*.js',
          'blocks/**/*.js',
          'Gruntfile.coffee'  # auto reload gruntfile config
        ]
        tasks: ['newer:concat:js']

      html:
        options:
          livereload: true
        files: [
          '*.html'
        ]

      images:
        files: [
          'blocks/**/*.{png,jpg,jpeg,gif}'
        ]
        tasks: ['copy:images']


    stylus:
      options:
        compress: false
        paths: ['blocks/']
        import: [
          'config.styl',
          'i-mixins/i-mixins__if-ie.styl',
          'i-mixins/i-mixins__clearfix.styl',
          'i-mixins/i-mixins__my.styl',
#          'i-mixins/i-mixins__vendor.styl',
#          'i-mixins/i-mixins__gradients.styl',
          'sprite_positions.styl'
        ]

      dev:
        expand: true
        cwd:    'blocks/'
        src:    [
          'i-reset/i-reset.styl',
          '_b-*/**/*.styl',
          'b-*/**/*.styl',
        ]
        dest: 'blocks'
        ext:  '.css'

      dev_ie:
        options:
          define:
            ie: true
        expand: true
        cwd:    'blocks/'
        src: [
          'i-reset/i-reset.styl',
          '_b-*/**/*.styl',
          'b-*/**/*.styl',
        ]
        dest: 'blocks'
        ext:  '.ie.css'
  
    autoprefixer:
      no_dest:
        src: 'publish/style.css'
        options:
          browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'ie 9']       #default
      single_file:
        options:
          browsers: ['ie 8', 'ie 7']
        src: 'publish/style.ie.css',
        dest: 'publish/style.ie.css'


    cssmin: {
      my_target: {
        files: [{
          expand: true,
          cwd: 'publish/',
          src: ['style.css'],
          dest: 'publish/',
          ext: '.css'
        },
        {
          expand: true,
          cwd: 'publish/',
          src: ['style.ie.css'],
          dest: 'publish/',
          ext: '.ie.css'
        }
        ]
      }
    }

    open:
      mainpage:
        path: 'http://localhost:8000/main.html';


  
  @registerTask( 'default',    [ 'concat:js', 'newer:stylus:dev', 'newer:stylus:dev_ie', 'newer:concat:css', 'newer:concat:css_ie', 'autoprefixer'])
  @registerTask( 'livereload', [ 'default', 'connect', 'open', 'watch' ])
#  @registerTask( 'publish',    [ 'jshint', 'concat:js', 'uglify', 'stylus:dev', 'stylus:dev_ie', 'concat:css', 'concat:css_ie','autoprefixer','cssmin'])
  @registerTask( 'prepublish', [ 'concat:js', 'stylus:dev', 'stylus:dev_ie', 'concat:css', 'concat:css_ie','autoprefixer']);
  @registerTask( 'publish',    [ 'prepublish', 'uglify', 'cssmin'])

  # copy images from /blocks to /publish and then compress them
  @registerTask( 'publish_img', [ 'clean', 'copy', 'imagemin' ])
